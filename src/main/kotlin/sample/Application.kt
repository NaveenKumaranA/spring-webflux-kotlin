package sample

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

// by default classes are final, but because of merging with spring open keyword is not required. Not a necessity to use open to extend the class when using Kotlin spring compiler plugin.
@SpringBootApplication
class Application

fun main(args: Array<String>) {
    runApplication<Application>(*args)
}
