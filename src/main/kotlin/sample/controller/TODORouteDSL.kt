package sample.controller

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.web.reactive.function.server.router

@Configuration
class TODORouteDSL(private val TODOService: TODOService) {

    @Bean
    fun apis() = router {
        (accept(APPLICATION_JSON) and "/todos").nest {
            GET("/", TODOService::getTodos)
            POST("/", TODOService::addTodo)
            GET("/{id}", TODOService::getTodo)
            PUT("/{id}", TODOService::updateTodo)
            DELETE("/{id}", TODOService::deleteTodo)
        }
    }

}