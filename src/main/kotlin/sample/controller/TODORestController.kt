package sample.controller;

import org.springframework.web.bind.annotation.*

@RestController("/rest/todos")
class TODORestController(private val TODOService: TODOService) {

    @GetMapping("/")
    fun all() = TODOService::getTodos

    @PostMapping("/")
    fun add() = TODOService::addTodo

    @GetMapping("/{id}")
    fun findOne() = TODOService::getTodo

    @PutMapping("/{id}")
    fun update() = TODOService::updateTodo

    @DeleteMapping("/{id}")
    fun delete() = TODOService::deleteTodo

}
