package sample.controller

import sample.domain.Todo
import org.springframework.http.HttpStatus.*
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.BodyInserters.fromObject
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono

@Component
open class TODOService {

    val map = linkedMapOf<String, Todo>()

    fun getTodos(req: ServerRequest): Mono<ServerResponse> = ServerResponse.ok().body(fromObject(map.values))


    fun getTodo(req: ServerRequest): Mono<ServerResponse> = Mono.just(map[req.pathVariable("id")]).flatMap { v -> ServerResponse.ok().body(fromObject(v)) }

    
    fun addTodo(req: ServerRequest): Mono<ServerResponse> {
        return req.bodyToMono(Todo::class.java).flatMap {
            m -> map[m.id] = m
            ServerResponse.status(CREATED).body(fromObject(m))
        }
    }

    fun updateTodo(req: ServerRequest): Mono<ServerResponse> {
        return req.bodyToMono(Todo::class.java).flatMap { m ->
            val id = req.pathVariable("id")
            m.id = id
            map[id] = m
            ServerResponse.status(ACCEPTED).body(fromObject(m))
        }
    }
    
    fun deleteTodo(req: ServerRequest): Mono<ServerResponse> {
        return Mono.just(req.pathVariable("id")).flatMap { id -> 
            map.remove(id)
            ServerResponse.status(ACCEPTED).build()
        }
    }
}