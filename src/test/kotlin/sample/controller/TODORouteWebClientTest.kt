package sample.controller

import org.junit.Test
import org.springframework.core.io.ClassPathResource
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.bodyFromPublisher
import org.springframework.web.reactive.function.server.ServerResponse.ok
import org.springframework.web.reactive.function.server.ServerResponse.status
import org.springframework.web.reactive.function.server.router
import sample.domain.Todo

class TODORouteWebClientTest {

    @Test
    fun testNested() {
        val routerFunction = router {
            ("/api" and accept(MediaType.APPLICATION_JSON) and contentType(MediaType.APPLICATION_JSON)).nest {
                POST("/create", { req ->
                    status(HttpStatus.CREATED).body(bodyFromPublisher(req.bodyToMono(Todo::class.java)))
                })
                PUT("/update", { req ->
                    status(HttpStatus.ACCEPTED).body(bodyFromPublisher(req.bodyToMono(Todo::class.java)))
                })
            }
        }

        val client = WebTestClient.bindToRouterFunction(routerFunction).build()

        client.post()
                .uri("/api/create")
                .body(BodyInserters.fromObject(Todo("1", "one")))
                .exchange()
                .expectStatus().isCreated
                .expectBody()
                .jsonPath("$.item")
                .isEqualTo("one")

        client.put()
                .uri("/api/update")
                .body(BodyInserters.fromObject(Todo("1", "one")))
                .exchange()
                .expectStatus().isAccepted
                .expectBody()
                .jsonPath("$.item")
                .isEqualTo("one")
    }
}