package sample.controller

import org.junit.Before
import org.junit.Test
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.web.reactive.function.BodyInserters
import sample.domain.Todo

class TODORouteDSLTest {
    
    lateinit var client: WebTestClient

    @Before
    fun setUp(): Unit {
        this.client = WebTestClient.bindToRouterFunction(TODORouteDSL(TODOService()).apis()).build()
    }

    @Test
    fun testMessages() {
        this.client.post()
                .uri("/todos")
                .body(BodyInserters.fromObject(Todo("1", "one")))
                .exchange()
                .expectStatus().isCreated
                .expectBody()
                .jsonPath("$.id")
                .isEqualTo(1)

        this.client.post()
                .uri("/todos")
                .body(BodyInserters.fromObject(Todo("2", "two")))
                .exchange()
                .expectStatus().isCreated
                .expectBody()
                .jsonPath("$.id")
                .isEqualTo(2)

        this.client.get()
                .uri("/todos")
                .exchange()
                .expectStatus().isOk
                .expectBody()
                .jsonPath("$.[0].id")
                .isEqualTo(1)

        this.client.delete()
                .uri("/todos/1")
                .exchange()
                .expectStatus().isAccepted

        this.client.get()
                .uri("/todos")
                .exchange()
                .expectStatus().isOk
                .expectBody()
                .jsonPath("$.[0].id")
                .isEqualTo(2)
    }
}